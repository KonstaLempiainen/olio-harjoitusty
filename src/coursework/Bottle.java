/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package coursework;

import javafx.scene.image.Image;

/**
 *
 * @author Konsta
 */
public class Bottle extends Item {
    //constructor
    public Bottle(){
        this.fragility = true;
        this.name = "Jallu";
        this.size = 1;
        this.itemImagePath = getClass().getResource("images/Jallupullo.jpg").toExternalForm();
        this.img = new Image(itemImagePath);
    }
    //toString method is overrided to make handling comboboxes easier
    @Override
    public String toString(){
        return this.name;
    }

}
