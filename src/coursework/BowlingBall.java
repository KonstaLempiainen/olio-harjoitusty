/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package coursework;

import javafx.scene.image.Image;

/**
 *
 * @author Konsta
 */
public class BowlingBall extends Item {
    
    //constructor
    public BowlingBall(){
        this.fragility = false;
        this.name = "Keilapallo";
        this.size = 1;
        this.itemImagePath = getClass().getResource("images/ball.jpg").toExternalForm();
        this.img = new Image(itemImagePath);
    }
    //toString method is overrided to make handling comboboxes easier
    @Override
    public String toString(){
        return this.name;
    }
}
