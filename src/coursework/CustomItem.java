/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package coursework;

import javafx.scene.image.Image;

/**
 *
 * @author Konsta
 */
public class CustomItem extends Item{
    //constructor
    public CustomItem(boolean fragility, String name, int size){
        this.fragility = fragility;
        this.name = name;
        this.size = size;
        this.itemImagePath = getClass().getResource("images/custom.jpg").toExternalForm();
        this.img = new Image(itemImagePath);
    }
    //toString method is overrided to make handling comboboxes easier
    @Override
    public String toString(){
        return this.name;
    }

}
