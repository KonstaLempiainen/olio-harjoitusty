/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package coursework;

import javafx.scene.image.Image;

/**
 *
 * @author Konsta
 */
public class FirstPackage extends PackageAbstract{
    //constructor
    public FirstPackage(){
        this.packageType = "1. Luokka";
        this.packageColor = "red";
        this.packageSpeed = 1;
        this.packageImagePath = getClass().getResource("images/red-package.jpg").toExternalForm();
        this.img = new Image(packageImagePath);
        
    }
    //contsructor for copying --- used for storing package to warehouse
    public FirstPackage(PackageAbstract pack){
        this.packageType =  pack.packageType;
        this.packageColor = pack.packageColor;
        this.packageSpeed = pack.packageSpeed;
        this.packageImagePath = getClass().getResource("images/red-package.jpg").toExternalForm();
        this.img = new Image(packageImagePath);
        
    }
    //function to check if item fits to package --- return value is handled in switch statement
    @Override
    protected int putItem(Item it){
        int result = 0;
        if(it.getFragility()){
            result = 1;
        }
        else{
            item = it;
            result = 0;        }
        
        return result;
    }
    //method for checking if iten breaks during trip --- not utilized in the program
    @Override
    protected boolean travel(){
        boolean state;
        double rnd = Math.random();
        if (rnd < 0.5){
            state = true;
        }
        else{
            state = false;
        }
        return state;
    }
    //toString method is overrided to make handling comboboxes easier
    @Override
    public String toString(){
        return packageType;
    }

}
