/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package coursework;

import javafx.scene.image.Image;

/**
 *
 * @author Konsta
 */
public abstract class Item {
    //all the items inherit properties from this abstract Item class
    protected boolean fragility;
    protected String name;
    protected int size;
    protected String itemImagePath;
    protected Image img;

    public boolean getFragility() {
        return fragility;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public String getItemImagePath() {
        return itemImagePath;
    }
}
