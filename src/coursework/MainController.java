package coursework;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;

/**
 * FXML Controller class
 *
 * @author Konsta
 */
public class MainController implements Initializable {

    @FXML
    private Tab mapTab;
    @FXML
    private Tab packageTab;
    @FXML
    private Tab Warehouse;
    

    /**
     * First idea was to have main controller to handle information between different controllers but not utilized in the program
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }    
    
}
