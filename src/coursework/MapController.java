/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursework;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Konsta
 * MapController handles all actions in Map.fxml
 */

public class MapController implements Initializable {

    @FXML
    private Button sendPackageButton;
    @FXML
    private ComboBox<SmartPost> startPostMenu;
    @FXML
    private ComboBox<SmartPost> endPostMenu;
    @FXML
    private WebView webView;
    @FXML
    private Button addToMapButton;
    @FXML
    private ComboBox<String> cityMenuStart;
    @FXML
    private ComboBox<String> cityMenuEnd;
    @FXML
    private Button deletePathsButton;
    @FXML
    private ComboBox<String> inventoryMenu;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Set<String> uniqueElements = new HashSet(); // This is used to get unique city name in cityMenuStart and cityMenuEnd ComboBoxes
        boolean checkSelectionStart = cityMenuStart.getSelectionModel().isEmpty(); // Used to check if city is selected
        boolean checkSelectionEnd = cityMenuEnd.getSelectionModel().isEmpty(); // Used to check if city is selected
        
        webView.getEngine().load(getClass().getResource("index.html").toExternalForm()); //loads the map for the background
        SmartPostHandler sph;
        
        try {
            sph = SmartPostHandler.getInstance();
            //for loop is used to create list of smartposts available
            for(int i = 0; i<sph.getArrList().size();i++){
                uniqueElements.add(sph.getArrList().get(i).getSmartPostCityName());
                startPostMenu.getItems().add(sph.getArrList().get(i));
                endPostMenu.getItems().add(sph.getArrList().get(i));
            }
            //for loop is used to create list of available cities
            String[] array = uniqueElements.toArray(new String[uniqueElements.size()]);
            for(int i = 0; i<array.length;i++){
                cityMenuStart.getItems().add(array[i]);
                cityMenuEnd.getItems().add(array[i]);
            }
            //typically not utilised in the program since no city is selected during initialize
            if(!checkSelectionStart){
                startPostMenu.getItems().clear();
                for(int i = 0; i<sph.getArrList().size();i++){
                    if(cityMenuStart.getValue().equals(sph.getArrList().get(i).getSmartPostCityName())){
                        startPostMenu.getItems().add(sph.getArrList().get(i));
                    }
                }
            }
            if(!checkSelectionEnd){
                endPostMenu.getItems().clear();
                for(int i = 0; i<sph.getArrList().size();i++){
                    if(cityMenuEnd.getValue().equals(sph.getArrList().get(i).getSmartPostCityName())){
                        endPostMenu.getItems().add(sph.getArrList().get(i));
                    }
                }
            }
            //catches exception if problem occurs when trying to read xml data for the snartposts
        } catch (IOException ex) {
            Logger.getLogger(MapController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    @FXML
    private void sendPackage(ActionEvent event) {
        //make the arraylist from the coordinates of selected smartposts
        ArrayList pathPoints = new ArrayList();
        PackageHandler p = PackageHandler.getPackageHandlerInstance();
        pathPoints.add(startPostMenu.getValue().getLatitude());
        pathPoints.add(startPostMenu.getValue().getLongitude());
        pathPoints.add(endPostMenu.getValue().getLatitude());
        pathPoints.add(endPostMenu.getValue().getLongitude());
        
        //when sending package check is package is selected from warehouse or not
        if(inventoryMenu.getSelectionModel().isEmpty()){
            //execute javascript from index.html depending on the selection of the package
            webView.getEngine().executeScript("document.createPath(" + pathPoints + ", '"+ p.ar.get(p.ar.size()-1).packageColor +"', "+ p.ar.get(p.ar.size()-1).packageSpeed +")");
        }
        else{
            webView.getEngine().executeScript("document.createPath(" + pathPoints + ", '"+ p.ar.get(inventoryMenu.getSelectionModel().getSelectedIndex()).packageColor +"', "+ p.ar.get(inventoryMenu.getSelectionModel().getSelectedIndex()).packageSpeed +")");
        }
    }
//this was used before moving to tab pane to open up new window --- not utilized in the program
//    private void openPackageManagement(ActionEvent event) {
//            
//        try {
//            Stage management = new Stage();
//            Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageManagement.fxml"));
//            Scene scene = new Scene(page);
//            
//            management.setScene(scene);
//            management.show();
//            
//        } catch (IOException ex) {
//            Logger.getLogger(MapController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }

    @FXML
    private void addToMap(ActionEvent event) {
       //excecute javascript from index.html to create start and end smartposts on map
        webView.getEngine().executeScript("document.goToLocation('" + startPostMenu.getValue().getAddress()+ "," +  startPostMenu.getValue().getSmartPostCityId() + " " + startPostMenu.getValue().getSmartPostCityName() +"','" + startPostMenu.getValue().getSmartPostoffice() + " " + startPostMenu.getValue().getAvailability() + "','red')");
        webView.getEngine().executeScript("document.goToLocation('" + endPostMenu.getValue().getAddress()+ "," +  endPostMenu.getValue().getSmartPostCityId() + " " + endPostMenu.getValue().getSmartPostCityName() +"','" + endPostMenu.getValue().getSmartPostoffice() + " " + endPostMenu.getValue().getAvailability() + "','blue')");
    }

    @FXML
    private void setLocations(ActionEvent event) {
         //function is used to create start post menu and end post menu depending on selected city on the city menus       
        
        Set<String> uniqueElements = new HashSet();
        boolean checkSelectionStart = cityMenuStart.getSelectionModel().isEmpty(); // check if city selected
        boolean checkSelectionEnd = cityMenuEnd.getSelectionModel().isEmpty();
        
        SmartPostHandler sph;
        
        try {
            sph = SmartPostHandler.getInstance();
            for(int i = 0; i<sph.getArrList().size();i++){
                uniqueElements.add(sph.getArrList().get(i).getSmartPostCityName());
                startPostMenu.getItems().add(sph.getArrList().get(i));
                endPostMenu.getItems().add(sph.getArrList().get(i));
            }
            String[] array = uniqueElements.toArray(new String[uniqueElements.size()]);
            for(int i = 0; i<array.length;i++){
                cityMenuStart.getItems().add(array[i]);
                cityMenuEnd.getItems().add(array[i]);
            }
            if(!checkSelectionStart){ // if selection city is not selected check selection start is true so if result is false this is excecuted
                //for loop only adds smartposts based on the selected city
                startPostMenu.getItems().clear();
                for(int i = 0; i<sph.getArrList().size();i++){
                    if(cityMenuStart.getValue().equals(sph.getArrList().get(i).getSmartPostCityName())){
                        startPostMenu.getItems().add(sph.getArrList().get(i));
                    }
                }
            }
            if(!checkSelectionEnd){// if selection city is not selected check selection start is true so if result is false this is excecuted
                //for loop only adds smartposts based on the selected city
                endPostMenu.getItems().clear();
                for(int i = 0; i<sph.getArrList().size();i++){
                    if(cityMenuEnd.getValue().equals(sph.getArrList().get(i).getSmartPostCityName())){
                        endPostMenu.getItems().add(sph.getArrList().get(i));
                    }
                }
            }
            //catch the exception which can happen when reading the smartpost data in xml
        } catch (IOException ex) {
            Logger.getLogger(MapController.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 

    @FXML
    //executes javascript that deletes already drawn paths from the map
    private void deletePaths(ActionEvent event) {
        webView.getEngine().executeScript("document.deletePaths()");
    }


    @FXML
    private void loadInventoryMenu(MouseEvent event) {
        //method checks the warehouse status to create menu which user can select which package will be sent
        PackageHandler ph = PackageHandler.getPackageHandlerInstance();
        inventoryMenu.getItems().clear();
        for(int i = 0; i<ph.getAr().size(); i++){
            inventoryMenu.getItems().add(ph.getAr().get(i).packageType + ": " + ph.getAr().get(i).item.name);
        }

    }

}
