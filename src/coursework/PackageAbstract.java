/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package coursework;

import javafx.scene.image.Image;

/**
 *
 * @author Konsta
 * 
 * This abstract class is inherited by all three package classes
 */
public abstract class PackageAbstract {
    
    protected String packageType;
    protected int packageSpeed;
    protected String packageColor;
    protected int packageSize;
    protected String packageImagePath;
    protected Image img;
    protected Item item;
    
    protected abstract boolean travel();
    protected abstract int putItem(Item item);
}
