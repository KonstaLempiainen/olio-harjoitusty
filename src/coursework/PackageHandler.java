/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package coursework;

import java.util.ArrayList;

/**
 *
 * @author Konsta
 * 
 * This class acts as a warehouse + helps with passing the information between different controllers
 */
public class PackageHandler {
    
    ArrayList<PackageAbstract> ar = new ArrayList();
    static private PackageHandler handler = null;

    public ArrayList<PackageAbstract> getAr() {
        return ar;
    }
    private PackageHandler(){
    }
    //methos is used to add package to warehouse
    public void addPackage(PackageAbstract box){
        ar.add(box);
    }
    
    //Singleton princible is utilized to only allow 1 object to be created from this class
    static PackageHandler getPackageHandlerInstance(){
        if(handler == null){
            handler = new PackageHandler();
        }
        return handler;
    }
}
