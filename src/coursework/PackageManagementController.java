/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursework;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;


/**
 * FXML Controller class
 *
 * @author Konsta
 * 
 * This controller is used to handle actions of PackageManagement.fxml
 */
public class PackageManagementController implements Initializable {

    @FXML
    private ComboBox<Item> itemList;
    @FXML
    private Button makePackageButton;
    @FXML
    private Label describePackageLabel;
    @FXML
    private ImageView itemImage;
    @FXML
    private ImageView packageImage;
    @FXML
    private ComboBox<PackageAbstract> packageTypeMenu;
    @FXML
    private Button selectPackageButton;
    @FXML
    private Button showItemButton;
    @FXML
    private TextField customItemName;
    @FXML
    private CheckBox fragileCheck;
    @FXML
    private RadioButton smallRadio;
    @FXML
    private RadioButton mediumRadio;
    @FXML
    private RadioButton largeRadio;
    @FXML
    private Button createCustomButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //initialize function is used to create menus for items and packages
        itemList.getItems().add(new Bottle());
        itemList.getItems().add(new Sofa());
        itemList.getItems().add(new Television());
        itemList.getItems().add(new BowlingBall());
        packageTypeMenu.getItems().add(new FirstPackage());
        packageTypeMenu.getItems().add(new SecondPackage());
        packageTypeMenu.getItems().add(new ThirdPackage());
    }    


    @FXML
    private void makePackage(ActionEvent event) {
        //When creating package new package is copied based on selected package type
        PackageAbstract temp;
        PackageHandler handler = PackageHandler.getPackageHandlerInstance();   
        PackageAbstract p = packageTypeMenu.getValue();
        //switch statement handles creation of package copy 
        switch(p.packageType){
            case "1. Luokka":
                temp = new FirstPackage(p);
                break;
            case "2. Luokka":
                temp = new SecondPackage(p);
                break;
            default:
                temp = new ThirdPackage(p);
        }
        //second switch statement handles feedback to user based on item fitting to package
        switch(temp.putItem(itemList.getValue())){
            case 0:
                describePackageLabel.setText("Paketti valmis lähetettäväksi.");
                handler.addPackage(temp); // newly created package is stored to warehouse
                break;
            case 1:
                describePackageLabel.setText("Kaikki särkyvät paketit hajoavat 1. luokassa.\nVaihda tavara tai pakettiluokka.");
                break;
            case 2:
                describePackageLabel.setText("Pakettiluokka 2 on tarkoitettu vain pienille ja keskisuurille paketeille.\nVaihda tavara tai pakettiluokka.");
                break;
            case 3:
                describePackageLabel.setText("Pakettiluokka 3 on tarkoitettu vain suurille paketeille,\nsillä muuten emme voi taata kuljetuksen turvallisuutta.\nVaihda tavara tai pakettiluokka.");
                break;
        }
        
    }

    @FXML
    private void selectPackage(ActionEvent event) {
        //puts the correct image based on the package type
        packageImage.setImage(packageTypeMenu.valueProperty().getValue().img);
    }

    @FXML
    private void showItem(ActionEvent event) {
        //shows correct image based on selected item
        itemImage.setImage(itemList.valueProperty().getValue().img);
    }

    @FXML
    private void createCustom(ActionEvent event) {
        //creates new custom item based on user inputs
        itemList.getItems().add(new CustomItem(fragileCheck.isSelected(), customItemName.getText(), radioCheck()));
    }
    //checks which size user selected
    private int radioCheck(){
        int result;
        if(smallRadio.isSelected()){
            result = 1;
        }
        else if(mediumRadio.isSelected())
            result = 2;
        else{
            result = 3;
        }
        return result;
    }
    
}
