/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package coursework;

import javafx.scene.image.Image;

/**
 *
 * @author Konsta
 */
public class SecondPackage extends PackageAbstract{
    //constructor
    public SecondPackage(){
        this.packageType = "2. Luokka";
        this.packageColor = "blue";
        this.packageSpeed = 2;
        this.packageImagePath = getClass().getResource("images/blue-package.jpg").toExternalForm();
        this.img = new Image(packageImagePath);
        
    }
    //contsructor for copying --- used for storing package to warehouse
    public SecondPackage(PackageAbstract pack){
        this.packageType =  pack.packageType;
        this.packageColor = pack.packageColor;
        this.packageSpeed = pack.packageSpeed;
        this.packageImagePath = getClass().getResource("images/red-package.jpg").toExternalForm();
        this.img = new Image(packageImagePath);
        
    }
    //checks if item fits to selected package
    @Override
    protected int putItem(Item item){
        int result = 0;
        if(item.getSize() > 2){
            result = 2;
        }
        else{
            this.item = item;
            result = 0;        }
        
        return result;
    }
    // used to check if item breaks during travel --- Not utilized in the program
    @Override
    protected boolean travel(){
        boolean state;
        double rnd = Math.random();
        if (rnd < 0.99){
            state = true;
        }
        else{
            state = false;
        }
        return state;
    }
    //used to make handling comboboxes easier
    @Override
    public String toString(){
        return packageType;
    }

}