/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package coursework;

/**
 *
 * @author Konsta
 */
public class SmartPost {
    private String smartPostCityName;
    private String smartPostCityId;
    private String smartPostoffice;
    private String latitude;
    private String longitude;
    private String availability;
    private String address;
    
    //Constructor for SmartPost
    public SmartPost(String name, String id, String office, String lat, String lng, String av, String adr){
        this.smartPostCityName = name;
        this.smartPostCityId = id;
        this.smartPostoffice = office;
        this.latitude = lat;
        this.longitude = lng;
        this.availability = av;
        this.address = adr;
    }
    //Getters for each attribute
    public String getSmartPostCityName() {
        return smartPostCityName;
    }

    public String getSmartPostCityId() {
        return smartPostCityId;
    }

    public String getSmartPostoffice() {
        return smartPostoffice;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getAvailability() {
        return availability;
    }

    public String getAddress() {
        return address;
    }


    @Override
    public String toString(){
        return smartPostoffice;
    }   
}
