/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package coursework;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Konsta
 * 
 * Class is used to read xml data from the internet and to store found smartpost objects
 */
public class SmartPostHandler {
    
    static private SmartPostHandler handler = null;
    private ArrayList<SmartPost> arrList;
    private Document doc;
    private HashMap<String, String> map;
    
    //getters for hashmap and arraylist
    public HashMap<String, String> getMap() {
        return map;
    }
    
    public ArrayList<SmartPost> getArrList() {
        return arrList;
    }
    //Constructor which reads the xml data
    private SmartPostHandler(String text){
        
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(text)));
            doc.getDocumentElement().normalize();
            
            map = new HashMap();
            arrList = new ArrayList();
            parseCurrentData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(SmartPostHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //SmartPostHandler is using Singleton principle so only 1 object can be created from this class
    static public SmartPostHandler getInstance() throws IOException{
        String urlText = "http://smartpost.ee/fi_apt.xml";
        String text = xmlHandler(urlText);
        if(handler == null){
            handler = new SmartPostHandler(text);
        }
        return handler;
    }
    //function adds smartpost to arraylist
    private void addSmartPost(String name, String id, String office, String lat, String lng, String av, String adr){
        arrList.add(new SmartPost(name, id, office, lat, lng, av, adr));
    }
    //used for reading xml type data
    private static String xmlHandler(String text) throws MalformedURLException, IOException{
        URL url = new URL(text);
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        String line;
        String content = "";
        
        while((line = br.readLine())!= null){
            content += line + "\n";
        }
        return content;
    }
    //used to parse xml data and to add the desired values to hashmap and arraylist
    private void parseCurrentData(){
        NodeList nodes = doc.getElementsByTagName("place");
        for(int i = 1; i < nodes.getLength();i++){
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            map.put(getValue("code", e), getValue("city", e));
            
            addSmartPost(getValue("city", e),getValue("code", e), getValue("postoffice", e), getValue("lat", e), getValue("lng", e), getValue("availability", e), getValue("address", e));
        }
    }
    //used to aid parseCurrentData function
    private String getValue(String tag, Element e){
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent(); // used to get the desired value out from xml data
    }
}
