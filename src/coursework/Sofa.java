/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package coursework;

import javafx.scene.image.Image;

/**
 *
 * @author Konsta
 */
public class Sofa extends Item {
    //constructor
    public Sofa(){
        this.fragility = false;
        this.name = "Sohva";
        this.size = 3;
        this.itemImagePath = getClass().getResource("images/Sohva.jpg").toExternalForm();
        this.img = new Image(itemImagePath);
    }
    //used to make handling comboboxes easier
    @Override
    public String toString(){
        return this.name;
    }

}
