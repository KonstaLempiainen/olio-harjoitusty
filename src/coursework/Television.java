/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package coursework;

import javafx.scene.image.Image;

/**
 *
 * @author Konsta
 */
public class Television extends Item {
    //constructor
    public Television(){
        this.fragility = true;
        this.name = "TV";
        this.size = 2;
        this.itemImagePath = getClass().getResource("images/curved-screen-tv.jpg").toExternalForm();
        this.img = new Image(itemImagePath);
    }
    //used to make handling comboboxes easier
    @Override
    public String toString(){
        return this.name;
    }
}
