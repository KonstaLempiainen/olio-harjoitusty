/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package coursework;

import javafx.scene.image.Image;

/**
 *
 * @author Konsta
 */
public class ThirdPackage extends PackageAbstract{
     //constructor
    public ThirdPackage(){
        this.packageType = "3. Luokka";
        this.packageColor = "yellow";
        this.packageSpeed = 3;
        this.packageImagePath = getClass().getResource("images/yellow-package.jpg").toExternalForm();
        this.img = new Image(packageImagePath);
        
    }
    //contsructor for copying --- used for storing package to warehouse
    public ThirdPackage(PackageAbstract pack){
        this.packageType =  pack.packageType;
        this.packageColor = pack.packageColor;
        this.packageSpeed = pack.packageSpeed;
        this.packageImagePath = getClass().getResource("images/red-package.jpg").toExternalForm();
        this.img = new Image(packageImagePath);
        
    }
    //checks if item fits to selected package
    @Override
    protected int putItem(Item item){
        int result = 0;
        if(item.getSize() < 2){
            result = 3;
        }
        else{
            this.item = item;
            result = 0;        }
        
        return result;
    }
    // used to check if item breaks during travel --- Not utilized in the program
    @Override
    protected boolean travel(){
        boolean state;
        double rnd = Math.random();
        if (rnd < 0.2){
            state = true;
        }
        else{
            state = false;
        }
        return state;
    }
    //used to make handling comboboxes easier
    @Override
    public String toString(){
        return packageType;
    }

}