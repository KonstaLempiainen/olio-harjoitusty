/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursework;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

/**
 * FXML Controller class
 *
 * @author Taisto
 * 
 * This class is used to handle actions from the Warehouse.fxml
 */
public class WarehouseController implements Initializable {

    @FXML
    private ListView<String> warehouseList;
    @FXML
    private Button loadInventoryButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @FXML
    private void loadInventory(ActionEvent event) {
        //function only loads current inventory or items in the warehouse using for loop
        PackageHandler ph = PackageHandler.getPackageHandlerInstance();
        warehouseList.getItems().clear();
        for(int i = 0; i<ph.getAr().size(); i++){
            warehouseList.getItems().add(ph.getAr().get(i).packageType + ": " + ph.getAr().get(i).item.name);
        }
    }
    
}
